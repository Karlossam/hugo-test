---
draft: false
title: "Introduccion a Modelos"
date: 2023-05-27T19:10:33+02:00
tags:
- web 
- scripting
---

Una vez hecha nuestra web base en [Crear una app con Django I](https://karlossam.gitlab.io/sloth-blog/posts/crear-una-app-con-django-i/), vamos a introducir el importante concepto de **Modelos**. 

En lineas generales, entiendo un modelo como una **abstraccion de un sistema complejo**. Esto nos permite simplificarlo para centrarnos unicamente en la parte que nos importa de este. Un ejemplo seria una persona; a esta la puede definir su nombre, edad, estatura, peso, nacionalidad, color de pelo, aficiones, estudios, y un larguisimo etc; pero a la hora de crear un usuario, podemos crear un modelo de esta en el que unicamente nos importe su nombre y email.

En el caso de [Django](https://karlossam.gitlab.io/sloth-blog/posts/django/), estos modelos son abstracciones de las **tablas en la base de datos, tratandolas como objetos** y facilitando increiblemente su uso. Vista un pelin la teoria vamos a verlo practicamente.

# Esquema de la BBDD

Antes de comenzar a crear modelos a lo loco, es interesante el hacernos una idea de lo que queremos llegar a conseguir mediante estos y plantear un breve esquema de la base de datos final que podemos conseguir.

Para esto tenemos una maravillosa web: https://erd.dbdesigner.net/. En esta podemos crear esquemas de una manera realmente intuitiva.

Como boceto muy cutre del objetivo que mas o menos podemos plantear tenemos:

![](https://karlossam.gitlab.io/sloth-blog/images/Pasted%20image%2020230527193715.png)

No soy ni mucho menos un experto en base de datos, de hecho no tengo ni idea del tema, asi que tampoco me voy a meter a explicarlo porque la voy a liar.

# Crear un modelo

Con este boceto hecho, ya tenemos una idea de los modelos que tenemos que crear: `Post` y `Tag`; y aproximadamente de sus campos, tipos de datos y relaciones. Ahora toca aplicarlo, y para ello vamos a crear un archivo `blog/models.py` al que le añadiremos lo siguiente:

```python
import uuid # Importamos uuid para generar id complejos

from django.contrib.auth.models import User # Importamos el modelo default de Users
from django.db import models # Importamos models que contiene numersas clases de interes

class Post(models.Model): # Definimos el modelo Post
	id = models.UUIDField(default=uuid.uuid4, unique=True, primary_key=True, editable=False)
	title = models.CharField(max_length=200)
	content = models.TextField(null=True, blank=True)
	tags = models.ManyToManyField('Tag', blank=True)
	author = models.ForeignKey(User, on_delete=models.CASCADE) #?
	created = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.title # Para que muestre el titulo en vez de el id del objeto

class Tag(models.Model): # Definimos el modelo Tag
	id = models.UUIDField(default=uuid.uuid4, unique=True, primary_key=True, editable=False)
	title = models.CharField(max_length=50)
	created = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.title # Para que muestre el titulo en vez de el id del objeto
```

En este caso el modelo de `User` no es necesario crearlo de momento debido a que ya lo trae [Django](https://karlossam.gitlab.io/sloth-blog/posts/django/) por defecto. Vamos a explicar un poco lo que sucede aqui dentro de `Post` porque el otro sencillamente replica:
1. Heredamos de `models.Model` para facilitar su creacion
2. Cada variable que definimos es un **campo** de la tabla.
	1. A cada uno de estos le aplicamos un **tipo** que viene ya con `models` como:
		1. `CharField`: conjunto de caracteres limitado en longitud
		2. `TextField`: texto sin limite
		3. `ManyToManyField`: establece una relacion **N-M** con otro modelo
		4. `ForeingKey`: establece una relacion **N-1** con otro modelo
		5. `DateTimeField`: fecha
	2. Cada uno de estos campos puede contener **atributos** que modifican su comportamiento:
		1. `default`: valor por defecto
		2. `unique`: no puede haber repetidos
		3. `primary_key`: establece el campo como clave primaria de la tabla
		4. `editable`: permite o no que se edite
		5. `max_length`: limite de caracteres
		6. `null`: permite o no que sea nulo en la DB
		7. `blank`: permite o no que en un formulario vaya vacio
		8. `on_delete`: cuando tenemos una relacion **N-1** define el comportamiento cuando se borra el objeto asociado al modelo del que depende. Podemos hacer `CASCADE` para que se borren todos o `SET_NULL` para que se conserven pero ponga en null el campo
		9. `auto_now_add`: con campos de fecha, añade automaticamente la fecha de ese momento.

Una vez los hemos definido, necesitamos aplicar estos cambios a la DB (y tendremos que hacerlo cada vez que editemos/creemos modelos):

```bash
python manage.py makemigrations
python manage.py migrate
```

# Django Shell

Con las migraciones realizadas, podemos acceder a una shell que nos permite interactuar con estos:

```bash
python manage.py shell
```

Aqui tenemos un proceso de acciones basicas:

```python
from blog import Post, Tag
from django.contrib.auth.models import User

# Seleccion
posts = Post.objects.all() # Seleccionar todos los posts
post = Post.objects.get(title='X') # Seleccionar 1 objeto
post = Post.objects.filter(title__startswith='p') # Filtrar por comienzo
post = Post.objects.exclude(title__startswith='p') # Excluir por comienzo

# Consulta
print(post.title) # Mostrar titulo
print(post.created) # Mostrar fecha de creacion
print(post.id) # Mostrar uuid

# Relaciones DB
print(user.post_set.all()) # 1-N
print(post.tags.all()) # N-M

# CRUD
## Create
p = Post(title="Title")
p.save()

# Update
p.title = "New Title"
p.save()

# Remove
p.delete()
```

La cantidad de opciones que tenemos aqui son muchisimo mayores, pero lo dejamos para la propia docu que con esta base ya podemos hacer muchisimo: https://docs.djangoproject.com/en/4.2/topics/db/queries/.

De todos modos es realmente importante familiarizarse con el uso de esta, dado que es exactamente la forma de la que usaremos mas tarde los modelos en vistas y templates.

# Django Admin

Tambien podemos interactuar con esto desde el panel de admin en http://127.0.0.1:8000/admin, pero antes debemos crear el superuser:

```bash
python manage.py createsuperuser
```

Y añadir los modelos a `blog/admin.py`:

```python
from .models import Post, Tag

admin.site.register(Post)
admin.site.register(Tag)
```

Creamos unos cuantos modelos y tags al azar para familiarizarnos con la interfaz

# Usar los modelos

Teniendo ya posts con los que trabajar, podemos pasar a usarlos en vistas. Vamos a `blog/views.py` lo modificamos para aplicar lo aprendido:

```python
from django.shortcuts import render
from .models import Post
...
def posts(request):
	posts = Post.objects.all() # Seleccionamos todos los posts
	context = {'posts': posts} # Creamos un contexto para el request
    return render(request, 'posts.html', context) # Le añadimos el contexto
```

Este contexto veremos a continuacion como lo usaremos, pero de momento vale con entender que le pasa los objetos al `render`.

Por otro lado, vamos a ir preparando la base para mostrar post individuales. Seguimos los pasos de antes:

1. HTML > `blog/templates/blog/post.html`

```html
<h1>Post Individual</h1>
```

2. Vista > `blog/views.py`

```python
...
def post(request, pk): # Añadimos el Primary Key
	post = Post.objects.get(id=pk) # Seleccionamos el post por id
	context = {'post': post}
    return render(request, 'post.html', context)
```

3. Ruta > `blog/urls.py`

```python
urlpatterns...
	path('post/<str:pk>', views.post, name="post"),
```

Aqui pequeño añadido que hacemos con `<str:pk>`, que basicamente indica que lo que vaya despues de `/post/` lo considere un string y lo asigne a la variable `pk`, que pasara como argumento a `views.post`.

Con esto hecho ya deberiamos poder navegar a 3 sitios distintos de la app:

- http://127.0.0.1:8000/blog/

![](https://karlossam.gitlab.io/sloth-blog/images/Pasted%20image%2020230528120526.png)

- http://127.0.0.1:8000/blog/posts

![](https://karlossam.gitlab.io/sloth-blog/images/Pasted%20image%2020230528120513.png)

- http://127.0.0.1:8000/blog/post/ + uuid de un post creado

![](https://karlossam.gitlab.io/sloth-blog/images/Pasted%20image%2020230528120500.png)