---
draft: false
title: "AD Hacking"
date: 2023-06-11T21:30:35+02:00
tags:
- hacking
- windows
---

Nueva serie en la que vamos a ir tocando los temas que ando trabajando, en concreto el tema de hacking en Active Directory.

Iremos poco a poco completando las distintas fases que nos podemos encontrar, en un principio partiendo de un *Asumed Breach* y ya iremos expandiendo segun necesidades. Partiendo de esta base, y como no podia ser de otra manera, comenzamos con nuestra amiga **Enumeracion**

1. [Enumeracion del Dominio con Powerview](https://karlossam.gitlab.io/sloth-blog/posts/enumeracion-de-dominio-con-powerview)
2. Test > [[Enumeracion de Dominio con Powerview]]