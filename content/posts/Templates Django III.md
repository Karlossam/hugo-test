---
draft: false
title: "Templates Django III"
date: 2023-05-30T07:19:01+02:00
tags:
- web
- scripting
---

Retomando donde lo dejamos en [Introduccion a Modelos con Django II](https://karlossam.gitlab.io/sloth-blog/posts/introduccion-a-modelos-con-django-ii/), vamos a comenzar ya a explorar el uso de Templates en [Django](https://karlossam.gitlab.io/sloth-blog/posts/django/), completando asi una breve introduccion a su patron de diseño: Model-View-**Template**.

Los templates son documentos **HTML que incorporan un lenguaje adicional que nos permiten aplicar logica** (bucles for, condicionales con if, etc), **usar variables, e incluso modularizar la estructura del HTML** (por ejemplo escribiendo el `navbar.html` solo en 1 archivo y luego importandolo en el resto).

Los datos que recibe el template dependen de la vista que le llama, asi que vamos a comenzar viendo como podemos pasarle informacion y ya luego como representarla.

```python
def home(request): # Reusamos la funcion
	blog_name = "Sloth Blog" # Definimos un nuevo dato para probar
	context = {'blog_name': blog_name} # Incluimos el dato en el diccionario de contexto
	return render(request, 'index.html', context) # Y le pasamos ese contexto al template
```

Hecho esto, podemos ir al template `index.html` y editarlo para que represente el dato:

```html
<h1>{{blog_name}}</h1>
```

Como podemos ver las variables se representan tal que `{{var}}`. Recomendable probar a cambiar el nombre de la variable y su valor tanto en la vista como el template para ver como reacciona.

Una vez hemos visto el uso mas basico vamos a ver algo un poquito mas avanzado usando ya los modelos que preparamos en el capitulo anterior.

# Modelos + Templates

Si recordamos el capitulo anterior [Introduccion a Modelos con Django II](https://karlossam.gitlab.io/sloth-blog/posts/introduccion-a-modelos-con-django-ii/), tenemos varias formar de consultar los objetos con este modelo:

```python
posts = Post.objects.all() # Todos
posts = Post.objects.get(id=pk) # 1 especifico
```

Para aplicarlo a los templates vale con incluir esto en la vista y pasarlo mediante el contexto:

```python
def posts(request):
	posts = Post.objects.all() # Seleccionamos todos los posts
	context = {'posts': posts} # Creamos un contexto para el request
	return render(request, 'blog/posts.html', context) # Le añadimos el contexto y ahora vemos eso de blog/

def post(request, pk): # Añadimos el Primary Key
	post = Post.objects.get(id=pk) # Seleccionamos el post por id
	context = {'post': post}
	return render(request, 'blog/post.html', context) # Le añadimos el contexto y ahora vemos eso de blog/
```

Y al tener un array de posts, podemos aplicar un bucle:

```jinja2
{% for post in posts %}

<p>{{post.title}} - {{post.author}} - <a href="{% url 'post' post.id %}">{{post.title}}</a></p>

{% endfor %}
```

Como podemos ver aqui, tenemos un patron que se repetira a lo largo de todo el lenguaje:
- `{{var}}`: doble corchete para variables
- `{% log %}...{% endlog %}`: corchete + porcentaje, cerrarndo tag para cualquier cosa de logica como bucles, condicionales, etc. Este tiene algunas excepciones, pero bue, en lineas generales cierralo.

# HTML Modular

Una de las cositas mas potentes que esto nos ofece es la posibilidad de importar otros HTML en el documento. Por ejemplo, pongamos que queremos que `main.html` aparezca en todas las paginas que se visiten en nuestro blog. Es tan sencillo como añadir lo siguiente por ejemplo en `posts.html`:

```jinja2
{% extends 'main.html' %}

{% block content %}

{% for post in posts %}
<p>{{post.title}} - {{post.author}} - <a href="{% url 'post' post.id %}">{{post.title}}</a></p>
{% endfor %}

{% endblock %}
```

Ahora podemos tratar `index.html` como un modulo que se importa en `posts.html`, pudiendo añadir un HTML valido, CSS, Javascript y todo lo que queramos. Podemos aplicarlo tambien a `post.html`

```jinja2
{% extends 'main.html' %}

{% block content %}
<h2>{{post.title}}</h2>
<p>{{post.content}}</p>
{% endblock %}
```

Veamos un par de modificaciones que podemos hacer a `index.html` que posiblemente continuemos en el siguiente capitulo para profundizar en el tema de crear una web chulona:

```html
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sloth Blog</title>
</head>
<body>
	<h1>Sloth Blog</h1>
	{% include 'navbar.html' %}
	
    {% block content %}

	{% endblock %}
	
	{% include 'navbar.html' %}
</body>
</html>
```

Creamos un """`navbar.html`""" para ver como afecta:

```html
<p>------------------------------------------------------------</p>
```

Y podremos ver que aparecen estos cambios en `posts.html`

En esto podemos y vamos a profundizar notablemente a lo largo de las tareas, pero por el momento nos valdra para entender los fundamentos sobre los que iremos construyendo.

Ahora que ya lo tenemos modular, podemos hacer un par de cambios para que todo quede colocadito en su sitio; por ejemplo, `posts.html` dentro de la app `blog`, pero `index.html`(que lo vamos a empezar a llamar `main.html`) y `navbar.html` se pueden mantener en la raiz del proyecto.

Cremaos pues la siguiente estructura:

```
sloth/
├── blog
│   └── templates
│       └── blog
│   		├── posts.html
│   		└── post.html
└── templates
	├── main.html
    └── navbar.html
```

Y tenemos que reflejarlo en las vistas cambiando el html por `blog/x.html` en el caso de los posts para que apunte al directorio correcto. Aunque si hicimos ya el cambio antes no hace falta.