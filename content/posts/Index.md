---
title: "Index"
date: 2023-05-14T20:29:34+02:00
draft: false
---

Blog que comenzamos por multitud de razones, pero asi de primeras me vienen 2 puntos relevantes:

- **Interno**: basandome en el concepto de Blooms Taxonomy, creo que si consigo enfocar este blog adecuadamente como una forma de enseñar/creacion, puede ayudar a mi aprendizaje enormemente

![Bloom's Taxonomy](https://cdn.vanderbilt.edu/vu-wp0/wp-content/uploads/sites/59/2019/03/27124326/Blooms-Taxonomy-650x366.jpg)

- **Externo**: al mismo tiempo que hacemos esto, abre la posibilidad de que alguien pueda sacar provecho de la informacion que publique. Teniendo en cuenta que es un objetivo que llevo teniendo tiempo en mente, eso que me llevo.

La idea (a diferencia del primer blog que comence) es condensar la informacion, y hacer de cada una de las entradas un conjunto cohesionado de ideas que resulten en una actividad en si misma. Por ejemplo:
- Como usar `argparse` en Python
- Desplegar una web basica con Django y conectarla con PostgreSQL
- Un walkthrough de Tryhackme/Hackthebox
- Como montar un homelab rapidamente con Docker
- Diferentes tecnicas y metodologias de Hacking

El objetivo de esto va a ser crear un repositorio de informacion destilada de mis apuntes que permitan realizar tareas concretas, y con suerte ir desarrollandolas en una especie de series, aprovechando en el proceso para reforzar lo aprendido y compartirlo con otras personas.

# Indice

## Scripting
- [Django](https://karlossam.gitlab.io/sloth-blog/posts/django/)

## Hacking
- [AD Hacking](https://karlossam.gitlab.io/sloth-blog/posts/ad-hacking)