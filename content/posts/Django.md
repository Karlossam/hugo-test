---
draft: false
title: "Django"
date: 2023-05-14T20:50:07+02:00
tags:
- web
- scripting
---

>Pequeño disclaimer: sinceramente no tengo ni puta idea de lo que estoy haciendo. Acabo de empezar mi aventura en el mundo de desarrollo web, por lo que no se tengan muy en cuenta las liadas; el feedback es bienvenido. 

Comenzamos la serie explorando este maravilloso framework que nos permite crear aplicaciones web mediante Python, yendo desde presentar una simple pagina a un conjunto de aplicaciones interconectadas con bases de datos, data brokers etc etc.

El git con el codigo lo tendremos aqui: [Django Sloth Blog](https://gitlab.com/Karlossam/django-sloth-blog)

Pero no nos aceleremos y comencemos por lo basico:

- [Crear una app con Django I](https://karlossam.gitlab.io/sloth-blog/posts/crear-una-app-con-django-i/)
- [Introduccion a Modelos con Django II](https://karlossam.gitlab.io/sloth-blog/posts/introduccion-a-modelos-con-django-ii/)