---
draft: false
title: "Crear una app con Django I"
date: 2023-05-14T20:41:45+02:00
tags:
- scripting
- web
---

# Setup y Conceptos

Vamos a empezar con el clasico Hello World para trabajar las bases de como arrancar un proyecto y una app, y ya iremos construyendo.

Comenzamos preparando el entorno:

```bash
python3 -m venv . # Creamos el entorno virtual
source bin/activate # Lo activamos
pip install django # Instalamos Django
django-admin startproject sloth # Creamos el proyecto
cd sloth
django-admin startapp blog # Creamos la app
```

Entendamos que representa esto de "proyecto" y "app":
- **Proyecto**: conjunto de apps que cumplen un objetivo común entre ellas. Podemos entenderlo como una jerarquia jefe-empelados practicamente, teniendo este una vision mas global y dando ordenes de quien tiene que hacer que
- **App**: parte de un proyecto encargada de realizar una tarea especifica. Siguiendo el ejemplo de jefe-empleado, estas van a recibir ordenes del proyecto y estan(o deberian) especializadas en realizar algo concreto. Por ejemplo, nuestra app "blog" no deberia gestionar la autenticacion de usuarios y sus perfiles.

Visto esto, se nos queda un sistema de ficheros tal que:

```bash
sloth/
├── manage.py
├── sloth # Proyecto
│   ├── asgi.py
│   ├── __init__.py
│   ├── settings.py # Configuraciones
│   ├── urls.py # Rutas
│   └── wsgi.py
└── blog # App
	├── admin.py
	├── apps.py
	├── __init__.py
	├── migrations
	│   └── __init__.py
	├── models.py
	├── tests.py
	└── views.py # Vistas
```

Por los comentarios vemos 3 conceptos que aun no hemos tocado. Entendamoslos antes de lanzarnos a escribir codigo:
- **Rutas**: al igual que en redes, reciben una direccion y se redirigen hacia un sitio. Por ejemplo, haremos una ruta que pille la direccion `blog/` y la redirija a la vista `home`
- **Vistas**: encargadas de recibir una peticion y devolver unos resultados, como un html u otra vista
- **Configuraciones**: permiten definir numerosos aspectos del proyecto, como apps, bases de datos, directorios concretos, etc.

Con esto en mente, procedemos a configurar nuestra primera app base en `settings.py` para que Django sepa que esta ahi:

```python
import os
...
INSTALLED_APPS = [
	...
    'blog.apps.BlogConfig',
]
```

# Modificando la app

## Templates

Este concepto lo incrustamos un poco de manera troyana, dado que lo veremos mas adelante. Por el momento nos vale con entender que es el contenido final que muestra nuestra web, al que nos redirige la vista de hecho.

Para configurarlo de manera basica nos vale con crear `templates/index.html` tal que:

```bash
sloth/
├─ blog # App
├── manage.py
├── sloth # Proyecto
└── templates
	└── index.html
```

Con un html cualquiera (`<h1>Sloth Blog Index</h1>` por ejemplo) e indicamos en `settings.py` la existencia de este directorio para que sepa donde buscar:

```python
...
TEMPLATES = [
    {
        ...
        'DIRS': [
            os.path.join(BASE_DIR, 'templates')
        ],
        ...
```

Teniendo ya el template creado, necesitamos una manera de llegar a el, para lo que usaremos la vista de `blog/views.py`:

```python
from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'index.html') 


def posts(request):
    return render(request, 'posts.html') 
```

Como vemos, las vistas podemos definirlas mediante funciones, toman como argumento el objeto de la peticion(`request`), y devuelven mediante `render` ese mismo objeto y el template correspondiente.

Esto ya dejaria establecida la **conexion entre las vistas y las templates**, y nos faltaria unicamente **conectar mediante rutas la peticion a la vista**.

Existe una manera ligeramente mas sencilla de hacerlo, pero por buenas practicas vamos a definir las rutas de cada app en su propio archivo, en este caso `blog/urls.py`:

```python
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('posts/', views.posts, name='posts'),
]
```

Definir una ruta es tan sencillo como añadir un item al array `urlpatterns` que consta de 3 elementos: patron de la url a matchear, vista a la que redirigir, y el argumento opcional `name` que nos ayudara enormemente a futuro. Ya veremos esto ultimo.

Para conectar las rutas del proyecto con las de la app tenemos que indicarlo en su propio `sloth/urls.py` mediante el metodo `include()`:

```python
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('blog/', include('blog.urls')),
]
```

Eso del admin ya lo iremos viendo mas adelante. 

Para que entendamos la logica que sigue todo esto, imaginemos que nos entra una peticion a `http://0.0.0.0:8000/blog/posts`:
1. Se elimina la parte del host y puerto, quedando el patron `blog/posts`
2. Procesa la peticion `sloth/urls.py`, viendo que la peticion contiene `blog/` por lo que se lo pasa a `blog/urls.py`, quedando el patron `posts/`
3. La app procesa la peticion, viendo que hace match con `posts/`, redirigiendo a la vista `views.home`
4. La vista `home` procesa la peticion, y como lo unico que tiene definido es devolver `index.html`, asi lo hace.

Finalmente, podemos arrancar el servidor mediante el comando:

```bash
python manage.py runserver 0.0.0.0:8000
```

Y visualizar el resultado accediendo a http://SRV_IP:PORT/blog:

![](Pasted%20image%2020230520234206.png)

En la siguiente seccion vamos a ver [Introduccion a Modelos II](https://karlossam.gitlab.io/sloth-blog/posts/introduccion-a-modelos-con-django-ii/)